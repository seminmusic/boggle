import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { ToasterModule } from "angular2-toaster";

import { AppComponent } from "./app.component";
import { CalculatorComponent } from "./calculator/calculator.component";
import { GameComponent } from "./game/game.component";

const routes: Route[] = [
	{
		path: "calculator",
		component: CalculatorComponent
	},
	{
		path: "game",
		component: GameComponent
	},
	{
		path: "**",
		redirectTo: "/"
	}
];

@NgModule({
	bootstrap: [AppComponent],
	declarations: [
		AppComponent,
		CalculatorComponent,
		GameComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		RouterModule.forRoot(routes),
		ToasterModule.forRoot()
	],
	providers: []
})
export class AppModule { }
