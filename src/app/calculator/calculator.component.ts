import { Component, OnInit } from "@angular/core";

import { ScoreUtility } from "../shared/utilities/score.utility";
import { Player, PlayerResult } from "../shared/models/player.models";

@Component({
	selector: "app-calculator",
	templateUrl: "./calculator.component.html",
	styleUrls: ["./calculator.component.css"]
})
export class CalculatorComponent implements OnInit {

	wordPoints: { word: any, points: number }[] = [];
	listScores: { words: any | any[], totalScore: number }[] = [];
	players: Player[] = [];
	playersResult: PlayerResult[] = [];

	constructor() {}

	ngOnInit() {
		const words = [1, null, true, "", "se", "sem", "semi", "  semin  ", "seminmu", "seminmus", "seminmusic"];
		words.forEach(w =>
			this.wordPoints.push({ word: w, points: ScoreUtility.getWordPoints(w as string) })
		);

		const lists = [
			1,
			null,
			true,
			"",
			"Semin",
			[],
			[1, null, true, "", "semin"],
			["aa", "aaa", "sema", "SemA", "  semin", "seminmusic"]
		];
		lists.forEach(list =>
			this.listScores.push({ words: list, totalScore: ScoreUtility.calculateScore(list as string[]) })
		);

		this.players.push(new Player(1, "First", ["aaa", "bbb", "ccc"]));
		this.players.push(new Player(2, "Second", ["cccccc", "ddd"]));
		this.players.push(new Player(3, "Third", ["eee", "Ccc", "fff"]));
		this.players.push(new Player(4, "Lucas", ["am", "bibble", "loo", "malarkey", "nudiustertian", "quire", "widdershins", "xertz", "bloviate", "pluto"]));
		this.players.push(new Player(5, "Clara", ["xertz", "gardyloo", "catty", "fuzzle", "mars", "sialoquent", "quire", "lollygag", "colly", "taradiddle", "snickersnee", "widdershins", "gardy"]));
		this.players.push(new Player(6, "Klaus", ["bumfuzzle", "wabbit", "catty", "flibbertigibbet", "am", "loo", "wampus", "bibble", "nudiustertian", "xertz"]));
		this.players.push(new Player(7, "Raphael", ["bloviate", "loo", "xertz", "mars", "erinaceous", "wampus", "am", "bibble", "cattywampus"]));
		this.players.push(new Player(8, "Tom", ["bibble", "loo", "snickersnee", "quire", "am", "malarkey"]));
		this.playersResult = ScoreUtility.calculateScores(this.players);
	}

	getScoreForPlayer(player: Player): number {
		return this.playersResult.find(r => r.playerId === player.id).score;
	}

}
