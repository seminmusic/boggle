export class Player {
	id: number;
	name: string;
	words: string[];

	constructor(id: number, name: string, words: string[]) {
		this.id = id;
		this.name = name;
		this.words = words;
	}
}

export class PlayerResult {
	playerId: number;
	playerName: string;
	score: number;
}
