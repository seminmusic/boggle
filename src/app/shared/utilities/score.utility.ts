import { Player, PlayerResult } from "../models/player.models";

export class ScoreUtility {

	/**
	 * Calculates total score based on Boggle rules (assume that all words given are valid)
	 * @param words Single word or array of words
	 * @returns {number} Total score
	 */
	public static calculateScore(words: string | string[]): number {
		// Create array from parameter
		const wordsArray: string[] = (words instanceof Array) ? words : [words];
		// Get only values of string type
		const filteredWords = wordsArray.filter(w => typeof w === "string");
		if (!filteredWords.length) {
			return 0;
		}
		// Find unique words (removed whitespace from both ends, case insensitive)
		const uniqueWords: string[] = [];
		for (const w of filteredWords) {
			if (uniqueWords.indexOf(w.trim().toLowerCase()) === -1) {
				uniqueWords.push(w.trim().toLowerCase());
			}
		}
		// Calculate score for every unique word and add to total score
		let totalScore = 0;
		for (const w of uniqueWords) {
			totalScore += this.getWordPoints(w);
		}
		return totalScore;
	}

	/**
	 * Get single word points based on Boggle rules (assume that word is valid)
	 * @param word Single word
	 * @returns {number} Word points
	 */
	public static getWordPoints(word: string): number {
		if (!word || typeof word !== "string") {
			return 0;
		}
		const trimWord = word.trim();
		if (trimWord.length < 3) {
			return 0;
		}
		if (trimWord.length >= 8) {
			return 11;
		}
		switch (trimWord.length) {
			case 3:
			case 4:
				return 1;
			case 5:
				return 2;
			case 6:
				return 3;
			case 7:
				return 5;
		}
	}

	/**
	 * Calculates total score for each player based on multiplayer Boggle rules (assume that all players words given are valid)
	 * @param players Array of Player models
	 * @returns Array of PlayerResult models
	 */
	public static calculateScores(players: Player[]): PlayerResult[] {
		const results = [];
		// In multiplayer game, only words that are unique to each player give points,
		// so we will exclude duplicated words from calculation
		const allWords: string[] = [].concat(...players.map(p => p.words.map(w => w.trim().toLowerCase())));
		const nonDuplicatedWords: string[] = [];
		for (const w of allWords) {
			if (allWords.indexOf(w) === allWords.lastIndexOf(w)) {
				nonDuplicatedWords.push(w);
			}
		}
		// For each player we will include only unique player's words in calculation
		for (const player of players) {
			const playerUniqueWords: string[] = player.words.filter(w => nonDuplicatedWords.indexOf(w.trim().toLowerCase()) !== -1);
			const playerResult: PlayerResult = new PlayerResult();
			playerResult.playerId = player.id;
			playerResult.playerName = player.name;
			playerResult.score = this.calculateScore(playerUniqueWords);
			results.push(playerResult);
		}
		return results;
	}

}
