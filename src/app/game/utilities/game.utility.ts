import { Dice } from "../models/dice.model";

export class GameUtility {

	public static createBoard(size: number): Dice[][] {
		const board: Dice[][] = [];
		const randomLetters: string[] = this.generateRandomLetters(size * size);
		let letterIndex = 0;
		for (let x = 0; x < size; x++) {
			board[x] = [];
			for (let y = 0; y < size; y++) {
				board[x][y] = new Dice(x, y, randomLetters[letterIndex]);
				letterIndex++;
			}
		}
		return board;
	}

	private static generateRandomLetters(numberOfLetters: number): string[] {
		const random: string[] = [];
		const letters = "abcdefghijklmnopqrstuvwxyz";
		for (let i = 0; i < numberOfLetters; i++) {
			const randomIndex: number = Math.floor(Math.random() * letters.length);
			random.push(letters.charAt(randomIndex));
		}
		return random;
	}

}
