import { Component, OnInit, ElementRef } from "@angular/core";
import { ToasterService } from "angular2-toaster";
import { Dice } from "./models/dice.model";
import { GameUtility } from "./utilities/game.utility";
import { ScoreUtility } from "../shared/utilities/score.utility";

@Component({
	selector: "app-game",
	templateUrl: "./game.component.html",
	styleUrls: ["./game.component.css"]
})
export class GameComponent implements OnInit {

	gameBoard: Dice[][];
	selectedDices: Dice[];
	lastSelectedDice: Dice;
	wordsList: string[];
	playEnabled: boolean;
	playTimeInSeconds: number = 3 * 60;
	currentDateTime: Date;
	gameInterval: any;

	constructor(private element: ElementRef, private toasterService: ToasterService) {}

	ngOnInit() {
		this.setInitialValues();
	}

	private setInitialValues() {
		this.gameBoard = GameUtility.createBoard(4);
		this.selectedDices = [];
		this.lastSelectedDice = null;
		this.wordsList = [];
		this.playEnabled = false;
		this.currentDateTime = new Date();
		this.currentDateTime.setHours(0, 0, this.playTimeInSeconds, 0);
	}

	private setGameInterval() {
		let seconds = this.playTimeInSeconds;
		this.gameInterval = setInterval(() => {
			this.currentDateTime.setSeconds(this.currentDateTime.getSeconds() - 1);
			seconds--;
			if (seconds === 0) {
				clearInterval(this.gameInterval);
				this.playEnabled = false;
			}
		}, 1000);
	}

	startGame() {
		this.setInitialValues();
		this.playEnabled = true;
		this.setGameInterval();
	}

	getTimerValue() {
		const min = this.currentDateTime.getMinutes().toString();
		const sec = this.currentDateTime.getSeconds().toString();
		return (min.length < 2 ? "0" + min : min) + ":" + (sec.length < 2 ? "0" + sec : sec);
	}

	getCurrentWord() {
		return this.selectedDices.reduce((acc, curr) => acc + curr.letter, "");
	}

	getCurrentScore() {
		return ScoreUtility.calculateScore(this.wordsList);
	}

	addToList() {
		this.wordsList.push(this.getCurrentWord());
		this.resetBoard();
	}

	resetBoard() {
		this.selectedDices = [];
		this.lastSelectedDice = null;
		const selectedDiceElements = this.element.nativeElement.querySelectorAll("td.selected") as HTMLElement[];
		selectedDiceElements.forEach(e => e.classList.remove("selected"));
	}

	diceClicked(dice: Dice, event: Event) {
		console.log(`Cell clicked (${dice.x}, ${dice.y})`);
		event.stopPropagation();
		if (!this.playEnabled) {
			return;
		}
		const targetElement = event.target as HTMLElement;
		const tdElement = targetElement.tagName.toLowerCase() === "div" ? targetElement.parentElement : targetElement;
		if (tdElement.classList.contains("selected")) {
			// Deselect
			const validDeselect = this.validateDiceDeselect(dice);
			if (validDeselect) {
				tdElement.classList.remove("selected");
				this.selectedDices.pop();
				this.lastSelectedDice = this.selectedDices[this.selectedDices.length - 1];
			} else {
				this.toasterService.pop("info", "Invalid de-select", "You can only de-select last chosen dice.");
			}
		} else {
			// Select
			const validSelect = this.validateDiceSelect(dice);
			if (validSelect) {
				this.lastSelectedDice = dice;
				this.selectedDices.push(dice);
				tdElement.classList.add("selected");
			} else {
				this.toasterService.pop("warning", "Invalid select", "Please select adjacent dice neighbouring the current one.");
			}
		}
	}

	private validateDiceSelect(dice: Dice): boolean {
		if (!this.lastSelectedDice) {
			return true;
		}
		const validX = [this.lastSelectedDice.x - 1, this.lastSelectedDice.x, this.lastSelectedDice.x + 1];
		const validY = [this.lastSelectedDice.y - 1, this.lastSelectedDice.y, this.lastSelectedDice.y + 1];
		return validX.some(x => x === dice.x) && validY.some(y => y === dice.y);
	}

	private validateDiceDeselect(dice: Dice): boolean {
		return dice === this.lastSelectedDice;
	}

}
