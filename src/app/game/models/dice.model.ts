export class Dice {
	x: number;
	y: number;
	letter: string;

	constructor(x: number, y: number, letter: string) {
		this.x = x;
		this.y = y;
		this.letter = letter;
	}
}
